package com.beawergames.tanksgame.server

import androidx.annotation.Keep

@Keep
data class TanksSplashResponse(val url : String)