package com.beawergames.tanksgame.screens

import com.badlogic.ashley.core.Engine
import com.badlogic.gdx.*
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.utils.ScreenUtils
import com.beawergames.tanksgame.systems.*

class GameScreen(private val game: Game): ScreenAdapter() {
    private var batch = SpriteBatch()
    private val camera = OrthographicCamera()
    private val engine = Engine()

    private val gameControlSystem = GameControlSystem(camera,1)
    private val uiSystem = UISystem(110).apply {
        setInterface(object : UISystem.UIInterface {
            override fun onPause() {
                gameControlSystem.pause()
            }

            override fun onResume() {
                gameControlSystem.resume()
            }

            override fun onRestart() {
                gameControlSystem.restart()
            }

            override fun onExit() {
                game.screen = MenuScreen(game)
            }

            override fun onShoot() {
                gameControlSystem.shoot()
            }

            override fun setPower(power: Int) {
                gameControlSystem.setPower(power)
            }

            override fun setAngle(angle: Int) {
                gameControlSystem.setAngle(angle)
            }

        })
    }

    init {
        camera.setToOrtho(false)

        gameControlSystem.setInterface(object : GameControlSystem.GameControlInterface {
            override fun setAngle(angle: Int) {
                uiSystem.setAngle(angle)
            }

            override fun setPower(power: Int) {
                uiSystem.setPower(power)
            }

            override fun setPlayer(currentPlayer: Boolean, life: Int) {
                uiSystem.setPlayer(currentPlayer, life)
            }

            override fun setWind(wind: Int) {
                uiSystem.setWind(wind)
            }

            override fun showWinner(winner: Boolean) {
                uiSystem.showWinScreen(winner)
            }
        })

        engine.addSystem(gameControlSystem)
        engine.addSystem(BulletSystem(camera, 10).apply { setInterface(object : BulletSystem.BulletInterface {
            override fun hit() {
                gameControlSystem.hit()
            }
            override fun animEnd() {
                gameControlSystem.switchPlayer()
            }
        }) })
        engine.addSystem(DrawSystem(batch, 100))
        engine.addSystem(EffectSystem(batch, 101))
        engine.addSystem(uiSystem)
    }

    override fun show() {
        val inputAdapter = object : InputAdapter() {

            override fun keyDown(keycode: Int): Boolean = when(keycode) {
                Input.Keys.TAB -> {
                    game.screen = MenuScreen(game)
                    true
                }
                Input.Keys.RIGHT -> {
                    camera.translate(0f, 20f)
                    true
                }
                Input.Keys.LEFT -> {
                    camera.translate(0f, -20f)
                    true
                }
                Input.Keys.DOWN -> {
                    camera.translate(-20f, 0f)
                    true
                }
                Input.Keys.UP -> {
                    camera.translate(20f, 0f)
                    true
                }
                else -> false
            }

            override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
                gameControlSystem.onScreenPress(screenX, screenY)
                return true
            }

            override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
                gameControlSystem.onReleasePress()
                return true
            }

            override fun touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean {
                gameControlSystem.onScreenPress(screenX, screenY)
                return true
            }
        }
        val inputMultiplexer = InputMultiplexer()
        inputMultiplexer.addProcessor(uiSystem.getStage())
        inputMultiplexer.addProcessor(inputAdapter)
        Gdx.input.inputProcessor = inputMultiplexer
    }

    override fun render(delta: Float) {
        ScreenUtils.clear(0.0f, 0.0f, 0.0f, 1.0f)
        camera.update()
        batch.projectionMatrix = camera.combined
        engine.update(delta)
    }

    override fun hide() {
        Gdx.input.inputProcessor = null
    }
}