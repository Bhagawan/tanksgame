package com.beawergames.tanksgame.screens

import com.badlogic.gdx.*
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.utils.ScreenUtils
import com.beawergames.tanksgame.TanksGame
import com.beawergames.tanksgame.assets.Assets

class MenuScreen(private val game: Game): ScreenAdapter() {
    private val stage = Stage()

    init {
        createMenu()
    }

    override fun show() {
        val inputAdapter = object : InputAdapter() {

            override fun keyDown(keycode: Int): Boolean = when(keycode) {
                Input.Keys.TAB -> {
                    game.screen = GameScreen(game)
                    true
                }
                else -> false
            }
        }
        val inputMultiplexer = InputMultiplexer()
        inputMultiplexer.addProcessor(inputAdapter)
        inputMultiplexer.addProcessor(stage)
        Gdx.input.inputProcessor = inputMultiplexer
    }

    override fun render(delta: Float) {
        ScreenUtils.clear(0.0f, 0.0f, 0.0f, 1.0f)
        stage.act(Gdx.graphics.deltaTime.coerceAtMost(TanksGame.PERFECT_DT))
        stage.draw()
    }

    override fun resize(width: Int, height: Int) {
        stage.viewport.update(width, height)
    }

    override fun hide() {
        Gdx.input.inputProcessor = null
        stage.clear()
    }

    override fun dispose() {
        stage.dispose()
    }

    private fun createMenu() {
        val skin = Assets.assetManager.get("skin/skin.json", Skin::class.java)
        val table = Table()
        table.setFillParent(true)
        table.background = skin.getDrawable("sky")
        stage.addActor(table)
        table.pad(20.0f)

        Gdx.graphics.width / 3.0f

        val headerTop = Label("Танчики", skin, "header")
        table.add(headerTop).expandX().top()
        table.row()
        table.add(Label("1-1", skin, "header")).expandX().top()
        table.row()

        val newGameButton = TextButton("Новая Игра", skin,"main_menu")
        newGameButton.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                game.screen = GameScreen(game)
            }
        })
        newGameButton.pad(0f, 50f, 20f, 50f)
        table.add(newGameButton).expand().bottom()
        table.row()

        val exitButton = TextButton("Выход",skin,"main_menu")
        exitButton.addListener( object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                Gdx.app.exit()
            }
        })
        exitButton.pad(0f, 50f, 20f, 50f)
        table.add(exitButton).expand().bottom().padBottom(50.0f)
        table.row()
        table.add().expandX()
    }
}