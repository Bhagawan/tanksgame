package com.beawergames.tanksgame.components

import com.badlogic.ashley.core.Component

class BulletComponent(val power: Int, val wind : Int): Component