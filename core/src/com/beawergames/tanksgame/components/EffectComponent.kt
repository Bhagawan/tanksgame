package com.beawergames.tanksgame.components

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool
import com.beawergames.tanksgame.assets.Assets

class EffectComponent(): Component {
    val effect : ParticleEffectPool.PooledEffect = Assets.effectPool.obtain()

    constructor(x: Float, y: Float): this() {
        effect.setPosition(x, y)
    }
}