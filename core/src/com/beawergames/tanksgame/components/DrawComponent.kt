package com.beawergames.tanksgame.components

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.scenes.scene2d.utils.Drawable

class DrawComponent(val textureRegion: TextureRegion?): Component {
   var drawable: Drawable? = null

    constructor(drawable: Drawable) : this(null) {
        this.drawable = drawable
    }
}