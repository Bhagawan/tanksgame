package com.beawergames.tanksgame.assets

import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.assets.loaders.ParticleEffectLoader
import com.badlogic.gdx.graphics.g2d.ParticleEffect
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.scenes.scene2d.ui.Skin

class Assets {
    companion object {
        val assetManager = AssetManager()
        lateinit var effectPool :  ParticleEffectPool

        fun init() {
            assetManager.load("skin/skin.atlas", TextureAtlas::class.java)
            assetManager.load("skin/skin.json", Skin::class.java)
            val partParameter = ParticleEffectLoader.ParticleEffectParameter()
            partParameter.atlasFile = "skin/skin.atlas"
            assetManager.load("explosion_effect", ParticleEffect::class.java, partParameter)

            while(!assetManager.update()) { println("loading assets")}
            effectPool = ParticleEffectPool((assetManager.get("explosion_effect", ParticleEffect::class.java)), 1, 10)

        }

        fun dispose() {
            assetManager.dispose()
            effectPool.free
        }
    }
}