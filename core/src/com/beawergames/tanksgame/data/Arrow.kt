package com.beawergames.tanksgame.data

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.beawergames.tanksgame.assets.Assets
import com.beawergames.tanksgame.components.DrawComponent
import com.beawergames.tanksgame.components.TransformComponent
import kotlin.math.acos
import kotlin.math.pow
import kotlin.math.sqrt

class Arrow : Entity() {
    private val texture = Assets.assetManager.get("skin/skin.json", Skin::class.java).getRegion("shoot_arrow")
    private val tC = TransformComponent(Gdx.graphics.width * 0.25f, Gdx.graphics.height / 3f + Gdx.graphics.height / 20, 50f, texture.regionHeight.toFloat())


    init {
        tC.origin.x = 0f
        add(tC)
    }

    fun setPointTo(x: Int, y: Int) {
        val finalX = if(tC.pos.x < Gdx.graphics.width) x else (Gdx.graphics.width * 2f + x).toInt()
        val finalY = if(y < tC.pos.y) tC.pos.y.toInt() else (y - texture.regionHeight * 0.5f).coerceAtLeast(tC.pos.y).toInt()
        if(getComponent(DrawComponent::class.java) == null) show()
        tC.width = sqrt((finalX - tC.pos.x).pow(2) + (finalY - tC.pos.y).pow(2))
        var a =  Math.toDegrees(acos((finalX - tC.pos.x) / tC.width).toDouble()).toFloat().coerceAtLeast(0f).coerceAtMost(180f)
        if(tC.pos.x < Gdx.graphics.width) a = a.coerceAtMost(90f) else if(a < 90) a = 90f
        tC.angle = a
    }

    fun setStartTo(p: Point) {
        tC.pos.x = p.x
        tC.pos.y = p.y
    }

    fun setSideTo(left: Boolean) = if(left) tC.pos.x = Gdx.graphics.width * 0.3f else tC.pos.x = Gdx.graphics.width * 2.7f

    private fun show() {
        add(DrawComponent(texture))
    }

    fun hide() {
        remove(DrawComponent::class.java)
    }

    fun getAngle() :Float {
        return tC.angle
    }

    fun getPower() : Int {
        return tC.width.toInt() / 10 * if(tC.pos.x < Gdx.graphics.width) 1 else -1
    }

    fun setPower(power : Int) {
        tC.width = power * 10f * if(tC.pos.x < Gdx.graphics.width) 1 else -1
    }

    fun setAngle(angle : Int) {
        tC.angle = if(tC.pos.x < Gdx.graphics.width) angle.toFloat() else 180f - angle
    }

    fun setAngle(angle : Float) {
        tC.angle = angle
    }
}