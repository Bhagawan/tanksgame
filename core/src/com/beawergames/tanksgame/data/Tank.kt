package com.beawergames.tanksgame.data

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.beawergames.tanksgame.assets.Assets
import com.beawergames.tanksgame.components.DrawComponent
import com.beawergames.tanksgame.components.TankComponent
import com.beawergames.tanksgame.components.TransformComponent
import kotlin.math.*

class Tank(x: Float, y: Float, private val leftSide: Boolean) {
    private val tankBody = Entity()
    private val tankTracks = Entity()
    private val tankTurret = Entity()
    private val turretTC : TransformComponent
    private val verticalOff: Float


    init {
        val skin = Assets.assetManager.get("skin/skin.json", Skin::class.java)
        val body = if(leftSide) skin.getSprite("tank_body") else skin.getSprite("tank_body_right")
        val tracks = skin.getSprite("tank_tracks")
        val turret = skin.getSprite("tank_turret")
        val width = Gdx.graphics.width * 0.15f
        val height = body.regionHeight * width / body.regionWidth
        val scale = height / body.regionHeight

        verticalOff = turret.height / 2

        if(leftSide) {
            tankBody.add(TransformComponent(x, y + tracks.regionHeight * 0.5f, 5f, width, height ))
            tankBody.add(DrawComponent(body))
            tankBody.add(TankComponent())

            tankTracks.add(TransformComponent(x + width * 0.1f, y, width * 0.8f, tracks.regionHeight * scale))
            tankTracks.add(DrawComponent(tracks))
            tankTracks.add(TankComponent())

            turretTC = TransformComponent(x + width * 0.7f, y + (height + tracks.height) * 0.5f , turret.width * scale, turret.height * scale)
            turretTC.origin.x = 0f
            tankTurret.add(turretTC)
            tankTurret.add(DrawComponent(turret))
        } else {
            body.flip(true, false)
            tracks.flip(true, false)
            turret.flip(true, false)
            tankBody.add(TransformComponent(x, y + tracks.regionHeight * 0.5f, 5f, width, height ))
            tankBody.add(DrawComponent(body))
            tankBody.add(TankComponent())

            tankTracks.add(TransformComponent(x + width * 0.1f, y, width * 0.8f, tracks.regionHeight * scale))
            tankTracks.add(DrawComponent(tracks))
            tankTracks.add(TankComponent())

            turretTC = TransformComponent(x + width * 0.3f, y + (height + tracks.height) * 0.5f , turret.width * scale, turret.height * scale)
            turretTC.origin.x = 0f
            turretTC.angle = 180f
            tankTurret.add(turretTC)
            tankTurret.add(DrawComponent(turret))
        }

    }

    fun addToEngine(engine: Engine) {
        engine.addEntity(tankBody)
        engine.addEntity(tankTracks)
        engine.addEntity(tankTurret)
    }

    fun pointTurretTo(x: Int, y: Int) : Float {
        val finalX = if(turretTC.pos.x < Gdx.graphics.width) x else (Gdx.graphics.width * 2f + x).toInt()
        val finalY = if(y < turretTC.pos.y) turretTC.pos.y.toInt() else (y - verticalOff).toInt()
        val w = sqrt((finalX - turretTC.pos.x).pow(2) + (finalY - turretTC.pos.y).pow(2))
        var a =  Math.toDegrees(acos((finalX - turretTC.pos.x) / w).toDouble()).toFloat().coerceAtLeast(0f).coerceAtMost(180f)
        if(turretTC.pos.x < Gdx.graphics.width) a = a.coerceAtMost(90f) else if(a < 90) a = 90f
        turretTC.angle = a
        return  a
    }

    fun changeTurretAngle(angle: Float) {
        turretTC.angle = if(leftSide) angle else 180 - angle
    }

    fun getAngle() : Float {
        return turretTC.angle
    }

    fun getTurretEnd(): Point {
        return Point(
            (turretTC.width * cos(Math.toRadians(turretTC.angle.toDouble())) + turretTC.pos.x).toFloat(),
            (turretTC.width * sin(Math.toRadians(turretTC.angle.toDouble())) + turretTC.pos.y - verticalOff * 2.5).toFloat()
        )
    }
}