package com.beawergames.tanksgame.data

data class Point(val x: Float, val y: Float)