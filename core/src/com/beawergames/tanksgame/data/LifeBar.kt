package com.beawergames.tanksgame.data

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.beawergames.tanksgame.assets.Assets

class LifeBar: Actor() {
    private var life = 100
    private val back = Assets.assetManager.get("skin/skin.json", Skin::class.java).getRegion("life_back")
    private val bar = Assets.assetManager.get("skin/skin.json", Skin::class.java).getRegion("life_level")

    override fun draw(batch: Batch?, parentAlpha: Float) {
        batch?.draw(back, x, y, width, height)
        val pad = width * 0.02f
        val lifeLength = (width - pad * 2) / 100 * life
        batch?.draw(bar, x + pad, y + pad, lifeLength, height - pad * 2)
    }

    fun setLife(life: Int) {
        this.life = life
    }
}