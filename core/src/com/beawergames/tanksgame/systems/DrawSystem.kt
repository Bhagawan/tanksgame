package com.beawergames.tanksgame.systems

import com.badlogic.ashley.core.ComponentMapper
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.utils.Array
import com.beawergames.tanksgame.components.DrawComponent
import com.beawergames.tanksgame.components.TransformComponent
import kotlin.math.sign

class DrawSystem(private val batch: SpriteBatch, priority: Int): IteratingSystem(Family.all(DrawComponent::class.java, TransformComponent::class.java).get(), priority) {
    private val tM = ComponentMapper.getFor(TransformComponent::class.java)
    private val dM = ComponentMapper.getFor(DrawComponent::class.java)

    private val queue = Array<Entity>()
    private val comparator =
        Comparator<Entity> { p0, p1 -> (tM.get(p0).pos.z - tM.get(p1).pos.z).sign.toInt() }

    override fun update(deltaTime: Float) {
        super.update(deltaTime)

        queue.sort(comparator)

        var dC: DrawComponent?
        var tC: TransformComponent?

        batch.begin()
        for (entity in queue) {
            dC = dM.get(entity)
            tC = tM.get(entity)
            if(dC.textureRegion != null) batch.draw(
                dC.textureRegion, tC.pos.x, tC.pos.y, tC.origin.x, tC.origin.y,
                tC.width, tC.height, tC.scale, tC.scale, tC.angle)
            dC.drawable?.draw(batch, tC.pos.x, tC.pos.y, tC.width, tC.height)
        }

        batch.end()

        queue.clear()
    }

    override fun processEntity(entity: Entity?, deltaTime: Float) {
        entity?.let { queue.add(it) }
    }

}