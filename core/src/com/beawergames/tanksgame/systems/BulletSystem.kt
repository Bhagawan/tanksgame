package com.beawergames.tanksgame.systems

import com.badlogic.ashley.core.ComponentMapper
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.math.Rectangle
import com.beawergames.tanksgame.TanksGame
import com.beawergames.tanksgame.components.*
import kotlin.math.absoluteValue
import kotlin.math.cos
import kotlin.math.sin

class BulletSystem(private val camera: Camera, priority: Int) : IteratingSystem(Family.all(TransformComponent::class.java, BulletComponent::class.java).get(), priority) {
    private val tM = ComponentMapper.getFor(TransformComponent::class.java)
    private val bM = ComponentMapper.getFor(BulletComponent::class.java)

    private var i: BulletInterface? = null

    override fun processEntity(entity: Entity?, deltaTime: Float) {
        val tC = tM.get(entity)
        val bC = bM.get(entity)
        entity?.let {
            if(it.getComponent(DrawComponent::class.java ) != null) {
                tC.angle = if(tC.angle <= 90) (tC.angle - 1f * deltaTime / TanksGame.PERFECT_DT).coerceAtLeast(-90f)
                else (tC.angle + 1f * deltaTime / TanksGame.PERFECT_DT).coerceAtMost(270f)
                val dX = (bC.power * cos(Math.toRadians(tC.angle.toDouble())).toFloat().absoluteValue + bC.wind)
                val dY = bC.power.absoluteValue * sin(Math.toRadians(tC.angle.toDouble())).toFloat()
                tC.pos.x += dX
                tC.pos.y += dY
                println("dX = $dX   dY = $dY  deltaTime = $deltaTime")
                if(camera.position.x in Gdx.graphics.width * 0.5f - 50..Gdx.graphics.width * 2.5f + 50) camera.translate(dX, dY, 0f)
                if(tC.pos.x in -100f..Gdx.graphics.width * 3f) {
                    val bRect = Rectangle(tC.pos.x, tC.pos.y, tC.width, tC.height)
                    val list = engine.getEntitiesFor(Family.one(TerrainComponent::class.java, TankComponent::class.java).get())
                    for(e in list) {
                        val eTC = tM.get(e)
                        val eRect = Rectangle(eTC.pos.x, eTC.pos.y, eTC.width, eTC.height)
                        if(bRect.overlaps(eRect)) {
                            it.remove(DrawComponent::class.java)
                            it.add(EffectComponent(tC.pos.x + tC.width * 0.5f, tC.pos.y + tC.height * 0.5f))
                            if(e.getComponent(TankComponent::class.java) != null) i?.hit()
                        }
                    }
                } else if(tC.pos.x !in -100f..Gdx.graphics.width * 3f){
                    engine.removeEntity(it)
                    i?.animEnd()
                }
            } else if(it.getComponent(EffectComponent::class.java) == null) {
                i?.animEnd()
                engine.removeEntity(it)
            }
        }
    }

    fun setInterface(int : BulletInterface) {
        i = int
    }

    interface BulletInterface {
        fun hit()
        fun animEnd()
    }

}