package com.beawergames.tanksgame.systems

import com.badlogic.ashley.core.ComponentMapper
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.beawergames.tanksgame.assets.Assets
import com.beawergames.tanksgame.components.EffectComponent
import com.beawergames.tanksgame.components.TransformComponent

class EffectSystem(private val bath: SpriteBatch, priority: Int):
    IteratingSystem(Family.all(EffectComponent::class.java).get(), priority) {

    private val eM = ComponentMapper.getFor(EffectComponent::class.java)
    private val tM = ComponentMapper.getFor(TransformComponent::class.java)

    override fun update(deltaTime: Float) {
        bath.begin()
        super.update(deltaTime)
        bath.end()
    }

    override fun processEntity(entity: Entity?, deltaTime: Float) {
        val eC = eM.get(entity)
        tM.get(entity)?.let {
            eC.effect.setPosition(it.pos.x + it.width / 2, it.pos.y + it.height / 2)
            }
        if(!eC.effect.isComplete) {
            eC.effect.draw(bath, deltaTime)
        } else {
            Assets.effectPool.free(eC.effect)
            entity?.remove(EffectComponent::class.java)
        }
    }
}