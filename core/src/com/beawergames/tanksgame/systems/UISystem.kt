package com.beawergames.tanksgame.systems

import com.badlogic.ashley.core.EntitySystem
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable
import com.beawergames.tanksgame.assets.Assets
import com.beawergames.tanksgame.data.LifeBar

class UISystem(priority: Int): EntitySystem(priority) {
    private val stage = Stage()
    private val menuDialog = createMenuDialog()
    private val powerLabel = Label("0", Assets.assetManager.get("skin/skin.json", Skin::class.java))
    private val angleLabel = Label("0", Assets.assetManager.get("skin/skin.json", Skin::class.java))
    private val windLabel = Label("0", Assets.assetManager.get("skin/skin.json", Skin::class.java))
    private val powerTable = createPowerTable()
    private val angleTable = createAngleTable()
    private val lifeBar = LifeBar()

    private var uiInterface: UIInterface? = null
    private var currentPlayer = GameControlSystem.LEFT

    init {
        createUI()
    }

    override fun update(deltaTime: Float) {
        stage.act()
        stage.draw()
    }

    private fun createUI() {
        val skin = Assets.assetManager.get("skin/skin.json", Skin::class.java)
        val table = Table()
        table.setFillParent(true)
        stage.addActor(table)
        table.pad(20.0f)
        stage.cancelTouchFocus()

        val topScreen = Table()

        val menuBSprite = skin.getSprite("button_settings").also {
            it.setSize(Gdx.graphics.height / 10.0f, Gdx.graphics.height / 10.0f)
        }
        val buttonPause = ImageButton(SpriteDrawable(menuBSprite))
        buttonPause.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                uiInterface?.onPause()
                menuDialog.show(stage)
            }
        })
        topScreen.add(buttonPause).top().left().width(Gdx.graphics.height / 10.0f).height(Gdx.graphics.height / 10.0f)

        topScreen.add(lifeBar).expand().center().pad(5f).size(Gdx.graphics.height * 0.8f, Gdx.graphics.height * 0.1f)
        val windTable = Table()
        windTable.background = skin.getDrawable("text_back")
        windTable.pad(5f)
        windTable.add(Label("Ветер", skin))
        windTable.row()
        windTable.add(windLabel)
        topScreen.add(windTable).center().right().size(Gdx.graphics.height / 5.0f, Gdx.graphics.height / 10.0f)

        table.add(topScreen).expandX().colspan(3).fillX()
        table.row()

        table.add(angleTable).bottom().left().expand().pad(10f).height(Gdx.graphics.height / 4.5f).width(Gdx.graphics.width / 3f)
        table.add(powerTable).center().expand().bottom().pad(10f).height(Gdx.graphics.height / 4.5f).width(Gdx.graphics.width / 3f)
        val shootButton = ImageButton(SpriteDrawable(skin.getSprite("button_fire").apply { setSize(Gdx.graphics.height / 4.0f, Gdx.graphics.height / 4.0f) }))
        shootButton.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                uiInterface?.onShoot()
            }
        })
        table.add(shootButton).expand().bottom().right().height(Gdx.graphics.height / 4.5f).pad(10f)

    }

    private fun createPowerTable() : Table {
        val skin = Assets.assetManager.get("skin/skin.json", Skin::class.java)
        val table = Table()
        table.background = skin.getDrawable("text_back")
        table.pad(5f, 15f, 10f,15f)
        table.add(Label("Сила",skin)).expandX().center().colspan(3)
        table.row()
        val minusButton = ImageButton(skin.getDrawable("button_minus")).also {
            it.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    val power = (powerLabel.text.toString().toInt() - 1).coerceAtLeast(0)
                    uiInterface?.setPower(power)
                    powerLabel.setText(power)
                }
            })
        }
        table.add(minusButton).bottom().left().size(Gdx.graphics.height / 6.0f, Gdx.graphics.height / 6.0f)
        table.add(powerLabel).expand().center().height(Gdx.graphics.height / 6.0f)
        val plusButton = ImageButton(skin.getDrawable("button_plus")).also {
            it.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    val power = (powerLabel.text.toString().toInt() + 1).coerceAtMost(1000)
                    uiInterface?.setPower(power)
                    powerLabel.setText(power)
                }
            })
        }
        table.add(plusButton).bottom().right().size(Gdx.graphics.height / 6.0f, Gdx.graphics.height / 6.0f)
        return table
    }

    private fun createAngleTable() : Table{
        val skin = Assets.assetManager.get("skin/skin.json", Skin::class.java)
        val table = Table()
        table.background = skin.getDrawable("text_back")
        table.pad(5f, 15f, 10f,15f)
        table.add(Label("Угол",skin)).expandX().center().colspan(3)
        table.row()
        val minusButton = ImageButton(skin.getDrawable("button_minus")).also {
            it.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    val angle = (angleLabel.text.toString().toInt() - 1).coerceAtLeast(0)
                    uiInterface?.setAngle(angle)
                    angleLabel.setText(angle)
                }
            })
        }
        table.add(minusButton).bottom().left().size(Gdx.graphics.height / 6.0f, Gdx.graphics.height / 6.0f)
        table.add(angleLabel).expand().center()
        val plusButton = ImageButton(skin.getDrawable("button_plus")).also {
            it.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    val angle = (angleLabel.text.toString().toInt() + 1).coerceAtMost(90)
                    uiInterface?.setAngle(angle)
                    angleLabel.setText(angle)
                }
            })
        }
        table.add(plusButton).bottom().right().size(Gdx.graphics.height / 6.0f, Gdx.graphics.height / 6.0f)
        return table
    }

    private fun createMenuDialog(): Dialog {
        val skin = Assets.assetManager.get("skin/skin.json", Skin::class.java)
        val menu = Dialog("", skin)
        menu.background = SpriteDrawable(skin.getSprite("menu_back").apply { setSize(Gdx.graphics.height / 3.0f, Gdx.graphics.height / 3.0f) })
        menu.contentTable.pad(50.0f)
        menu.contentTable.add(Label("Пауза", skin, "header")).center().expand().colspan(2)
        menu.contentTable.row()

        val exitButton = ImageButton(SpriteDrawable(skin.getSprite("button_close").apply { setSize(Gdx.graphics.height / 6.0f, Gdx.graphics.height / 6.0f) })).also {
            it.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    uiInterface?.onExit()
                }
            })
        }
        menu.contentTable.add(exitButton).center().bottom().expandX().size(
            Gdx.graphics.height / 6.0f,
            Gdx.graphics.height / 6.0f)
        val continueButton = ImageButton(SpriteDrawable(skin.getSprite("button_continue").apply { setSize(Gdx.graphics.height / 6.0f, Gdx.graphics.height / 6.0f) })).also {
            it.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    menu.hide()
                    uiInterface?.onResume()
                }
            })
        }
        menu.contentTable.add(continueButton).center().bottom().expandX().size(
            Gdx.graphics.height / 6.0f,
            Gdx.graphics.height / 6.0f)

        return menu
    }

    private fun createEndDialog(winner: Boolean): Dialog {
        val skin = Assets.assetManager.get("skin/skin.json", Skin::class.java)
        val menu = Dialog("", skin)
        menu.background = SpriteDrawable(skin.getSprite("menu_back").apply { setSize(Gdx.graphics.height / 3.0f, Gdx.graphics.height / 3.0f) })
        menu.contentTable.pad(50.0f)
        val text = if(winner == GameControlSystem.LEFT) "Зеленый победил!" else "Синий победил!"
        menu.contentTable.add(Label(text, skin, "header")).center().expand().colspan(2)
        menu.contentTable.row()

        val exitButton = ImageButton(SpriteDrawable(skin.getSprite("button_close").apply { setSize(Gdx.graphics.height / 6.0f, Gdx.graphics.height / 6.0f) })).also {
            it.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    uiInterface?.onExit()
                }
            })
        }
        menu.contentTable.add(exitButton).center().bottom().expandX().size(
            Gdx.graphics.height / 6.0f,
            Gdx.graphics.height / 6.0f)
        val continueButton = ImageButton(SpriteDrawable(skin.getSprite("button_restart").apply { setSize(Gdx.graphics.height / 6.0f, Gdx.graphics.height / 6.0f) })).also {
            it.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    menu.hide()
                    uiInterface?.onRestart()
                }
            })
        }
        menu.contentTable.add(continueButton).center().bottom().expandX().size(
            Gdx.graphics.height / 6.0f,
            Gdx.graphics.height / 6.0f)

        return menu
    }

    private fun rearrangeTable() {
        val shootButton = ImageButton(SpriteDrawable(Assets.assetManager.get("skin/skin.json", Skin::class.java).getSprite("button_fire").apply { setSize(Gdx.graphics.height / 4.0f, Gdx.graphics.height / 4.0f) }))
        shootButton.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                uiInterface?.onShoot()
            }
        })

        val cells = (stage.actors.get(0) as Table).cells
        (cells[1] as Cell<Actor>).setActor(null)
        (cells[2] as Cell<Actor>).setActor(null)
        (cells[3] as Cell<Actor>).setActor(null)
        if(currentPlayer == GameControlSystem.LEFT) {
            (cells[1] as Cell<Actor>).setActor(angleTable)
            cells[1].width(Gdx.graphics.width / 3f)
            (cells[2] as Cell<Actor>).setActor(powerTable)
            cells[2].width(Gdx.graphics.width / 3f)
            (cells[3] as Cell<Actor>).setActor(shootButton)
            cells[3].width(Gdx.graphics.height / 4.5f)
        } else {
            (cells[1] as Cell<Actor>).setActor(shootButton)
            cells[1].width(Gdx.graphics.height / 4.5f)
            (cells[2] as Cell<Actor>).setActor(angleTable)
            cells[2].width(Gdx.graphics.width / 3f)
            (cells[3] as Cell<Actor>).setActor(powerTable)
            cells[3].width(Gdx.graphics.width / 3f)
        }
    }

    fun setInterface(i: UIInterface) {
        uiInterface = i
    }

    fun setPower(power: Int) {
        powerLabel.setText(power)
    }

    fun setAngle(angle: Int) {
        angleLabel.setText(angle)
    }

    fun setPlayer(left: Boolean, life: Int) {
        currentPlayer = left
        lifeBar.setLife(life)
        powerLabel.setText(0)
        angleLabel.setText(0)
        rearrangeTable()
    }

    fun getStage(): Stage {
        return stage
    }

    fun setWind(wind: Int) {
        windLabel.setText(wind)
    }

    fun showWinScreen(winner: Boolean) {
        createEndDialog(winner).show(stage)
    }

    interface UIInterface {
        fun onPause()
        fun onResume()
        fun onRestart()
        fun onExit()
        fun onShoot()
        fun setPower(power: Int)
        fun setAngle(angle: Int)
    }
}