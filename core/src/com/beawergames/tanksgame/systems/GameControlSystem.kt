package com.beawergames.tanksgame.systems

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.EntitySystem
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.beawergames.tanksgame.assets.Assets
import com.beawergames.tanksgame.components.BulletComponent
import com.beawergames.tanksgame.components.DrawComponent
import com.beawergames.tanksgame.components.TerrainComponent
import com.beawergames.tanksgame.components.TransformComponent
import com.beawergames.tanksgame.data.Arrow
import com.beawergames.tanksgame.data.Point
import com.beawergames.tanksgame.data.Tank
import kotlin.math.absoluteValue
import kotlin.math.cos
import kotlin.random.Random

class GameControlSystem(private val camera: Camera, priority: Int): EntitySystem(priority) {
    private lateinit var leftTank : Tank
    private lateinit var rightTank : Tank
    private var leftLife = 100
    private var rightLife = 100

    private val arrow = Arrow()

    companion object {
        const val LEFT = true
        const val RIGHT = false
        const val STATE_PLAYING = 0
        const val STATE_CAMERA = 1
        const val STATE_PAUSE = 2
    }

    private var currentState = STATE_PLAYING
    private var stateToResume = STATE_PLAYING
    private var currentPLayer = LEFT
    private var wind = Random.nextInt(-20, 20)

    private var camDX = 0f
    private var camDY = 0f

    private var gameInterface: GameControlInterface? = null

    override fun addedToEngine(engine: Engine?) {
        super.addedToEngine(engine)
        engine?.run { createWorld() }
        gameInterface?.setWind(wind)
    }

    override fun update(deltaTime: Float) {
        if(currentState == STATE_CAMERA) {
            camera.translate(camDX, camDY, 0f)
            if((camera.position.x - Gdx.graphics.width * if(currentPLayer == RIGHT) 2.5f else 0.5f).absoluteValue <= 20 &&
                    (camera.position.y - Gdx.graphics.height * 0.5f).absoluteValue <= 20) {
                camera.position.set(Gdx.graphics.width * if(currentPLayer == RIGHT) 2.5f else 0.5f, Gdx.graphics.height * 0.5f, 0f)
                currentState = STATE_PLAYING
                wind = Random.nextInt(-20, 20)
                gameInterface?.setWind(wind)
            }
        }
    }

    private fun createWorld() {
        val skin = Assets.assetManager.get("skin/skin.json", Skin::class.java)

        val terrainTop = Entity()
        val tTop = skin.getTiledDrawable("terrain_grass")

        leftTank = Tank(Gdx.graphics.width / 20f, Gdx.graphics.height / 3f - tTop.region.regionHeight * 3, true)
        rightTank = Tank(Gdx.graphics.width * 3 - Gdx.graphics.width * 0.4f + Gdx.graphics.width / 20f, Gdx.graphics.height / 3f - tTop.region.regionHeight * 3, false)

        tTop.scale = Gdx.graphics.height / 30f / tTop.region.regionHeight
        val grassTC = TransformComponent(-100f,Gdx.graphics.height* 0.3f - tTop.region.regionHeight.toFloat(), 2f, Gdx.graphics.width * 3f + 200, tTop.region.regionHeight * tTop.scale)
        terrainTop.add(grassTC)
        terrainTop.add(DrawComponent(tTop))
        terrainTop.add(TerrainComponent())
        engine.addEntity(terrainTop)

        val terrain = Entity()
        terrain.add(TransformComponent(-100f,-Gdx.graphics.height * 0.5f, 2f,Gdx.graphics.width * 3f + 200, Gdx.graphics.height * 0.8f - tTop.region.regionHeight.toFloat()))
        terrain.add(DrawComponent(skin.getDrawable("terrain_color_texture")))
        terrain.add(TerrainComponent())
        engine.addEntity(terrain)

        val sky = Entity()
        sky.add(TransformComponent(-100f,Gdx.graphics.height* 0.3f, 0f,Gdx.graphics.width * 3f + 200, Gdx.graphics.height* 4f))
        sky.add(DrawComponent(skin.getRegion("sky")))
        engine.addEntity(sky)

        leftTank.addToEngine(engine)
        rightTank.addToEngine(engine)
        engine.addEntity(arrow)

    }

    fun onScreenPress( x : Int, y : Int) {
        if(currentState == STATE_PLAYING) {
            if(currentPLayer == LEFT) {
                leftTank.pointTurretTo(x, Gdx.graphics.height - y)
                val p : Point = leftTank.getTurretEnd()
                arrow.setStartTo(p)
            } else {
                rightTank.pointTurretTo(x, Gdx.graphics.height - y)
                val p : Point = rightTank.getTurretEnd()
                arrow.setStartTo(p)
            }
            arrow.setPointTo(x, Gdx.graphics.height - y)
            var angle = arrow.getAngle().toInt()
            if(angle > 90) angle = 180 - angle
            arrow.setAngle(if(currentPLayer == LEFT) leftTank.getAngle() else rightTank.getAngle())
            gameInterface?.setAngle(angle)
            gameInterface?.setPower(arrow.getPower().absoluteValue)
        }
    }

    fun onReleasePress() {

    }

    fun hit() {
        if(currentPLayer == LEFT) rightLife = (rightLife - 30).coerceAtLeast(0)
        else leftLife = (leftLife - 30).coerceAtLeast(0)
        if(leftLife == 0) gameInterface?.showWinner(RIGHT)
        else if(rightLife == 0) gameInterface?.showWinner(LEFT)
    }

    fun setInterface(i: GameControlInterface) {
        gameInterface = i
    }

    fun switchPlayer() {
        currentState = STATE_CAMERA
        currentPLayer = !currentPLayer
        camDX = if(currentPLayer == RIGHT) (Gdx.graphics.width * 2.5f - camera.position.x) / 60
            else (Gdx.graphics.width * 0.5f - camera.position.x) / 60
        camDY = (Gdx.graphics.height * 0.5f - camera.position.y) / 60
        arrow.setSideTo(currentPLayer)
        if(currentPLayer == LEFT) {
            leftTank.changeTurretAngle(0f)
        } else rightTank.changeTurretAngle(0f)
        arrow.setAngle(if(currentPLayer == LEFT) 0f else 180f)
        arrow.setPower(if(currentPLayer == LEFT) 50 else -50)
        gameInterface?.setPlayer(currentPLayer, if(currentPLayer == LEFT) leftLife else rightLife)
    }

    fun pause() {
        stateToResume = currentState
        currentState = STATE_PAUSE
    }

    fun resume() {
        currentState = stateToResume
    }

    fun setPower(power : Int) {
        arrow.setPower(if(currentPLayer == LEFT) power else -power)
    }

    fun shoot() {
        arrow.hide()
        val bullet = Entity()
        bullet.add(DrawComponent(Assets.assetManager.get("skin/skin.json", Skin::class.java).getRegion("tank_bullet")))
        bullet.add(BulletComponent(arrow.getPower() * 1200 / Gdx.graphics.width, wind))

        val p = if(currentPLayer == LEFT) leftTank.getTurretEnd() else rightTank.getTurretEnd()
        val tC = TransformComponent(p.x, p.y + 40 , 50f, 40f)
        if(currentPLayer == RIGHT) tC.pos.x += cos(rightTank.getAngle()) * 20
        tC.angle = arrow.getAngle()
        bullet.add(tC)
        engine.addEntity(bullet)
    }

    fun setAngle(angle : Int) {
        if(currentPLayer == LEFT) {
            leftTank.changeTurretAngle(angle.toFloat())
            val p = leftTank.getTurretEnd()
            arrow.setStartTo(p)
            arrow.setAngle(angle)
        } else {
            rightTank.changeTurretAngle(angle.toFloat())
            val p = rightTank.getTurretEnd()
            arrow.setStartTo(p)
            arrow.setAngle(angle)
        }
    }

    fun restart() {
        currentState = STATE_PLAYING
        currentPLayer = RIGHT
        switchPlayer()
        leftLife = 100
        rightLife = 100
    }

    interface GameControlInterface {
        fun setAngle(angle : Int)
        fun setPower(power : Int)
        fun setPlayer(currentPlayer: Boolean, life: Int)
        fun setWind(wind: Int)
        fun showWinner(winner: Boolean)
    }


}