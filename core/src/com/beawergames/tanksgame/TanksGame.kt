package com.beawergames.tanksgame

import com.badlogic.gdx.Game
import com.beawergames.tanksgame.assets.Assets
import com.beawergames.tanksgame.screens.MenuScreen

class TanksGame : Game() {

	companion object {
		const val PERFECT_DT: Float = (16.67 * 2 / 1000).toFloat()
	}

	override fun create () {
		Assets.init()
		setScreen(MenuScreen(this))
	}

	override fun dispose () {
		Assets.dispose()
	}
}
